package com.sportcaller.webapp

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.*
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //enable debug
        WebView.setWebContentsDebuggingEnabled(true)

        if (savedInstanceState != null) {
            webView.restoreState(savedInstanceState);
        } else {
            initWebView()
        }

        goBtn.setOnClickListener {
            closeKeyboard()
            openUrl()
        }
    }

    private fun openUrl() {
        if (webAddressEt.text.isNotEmpty()) {
            if (webAddressEt.text.startsWith("https://")) {
                webView.loadUrl(webAddressEt.text.toString())
            } else {
                webView.loadUrl("https://" + webAddressEt.text.toString())
            }
        }
    }

    private fun initWebView() {
        webView.settings.javaScriptEnabled = true
        webView.settings.useWideViewPort = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.setSupportZoom(true)
        webView.settings.setSupportMultipleWindows(true)
        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webView.setBackgroundColor(Color.WHITE)

        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                progressBar.progress = newProgress
            }
        }

        webView.settings.domStorageEnabled = true
        webView.webViewClient = object : WebViewClient() {
            @RequiresApi(Build.VERSION_CODES.M)
            override fun onReceivedError(
                    view: WebView, request: WebResourceRequest, error: WebResourceError
            ) {
                showAlert(errorText = error.description.toString())
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progressBar.visibility = ProgressBar.INVISIBLE
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                progressBar.visibility = ProgressBar.VISIBLE
            }
        }
    }

    override fun onBackPressed() {
        return if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }
    }

    fun showAlert(errorText: String) {
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("ERROR")
            setMessage(errorText)
            setPositiveButton("OK") { dialog, _ ->
                dialog.dismiss()
            }
            show()
        }
    }

    private fun closeKeyboard() {
        val view = currentFocus
        view?.let { v ->
            val imm =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }
}
